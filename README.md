# man-pages

Flask application to render man pages for Wikimedia Toolforge infrastructure.

## Licenses

The main application is copyright (c) 2021, Taavi Väänänen.
Please see `LICENSE` for the license.

The included `roffit` Perl script is copyright (c) 2004 - 2018, Daniel
Stenberg <daniel@haxx.se>. Please see https://github.com/bagder/roffit/blob/master/LICENSE
for the license.

The included font files in `static/` are copyright (c) 2015-2021,
Renzhi Li. Please see https://github.com/be5invis/Iosevka/blob/master/LICENSE.md
for the license.
