import collections
import pathlib
import subprocess

from flask import Flask, render_template

from man_pages import db

app = Flask(__name__)


@app.route("/")
def hello_world():
    with db.connection() as conn:
        all_pages = conn.query_all(
            "SELECT mp_distro, mp_section, mp_title FROM man_page;"
        )

    pages_by_distro = collections.defaultdict(set)
    for page in all_pages:
        pages_by_distro[page["mp_distro"]].add(
            f"{page['mp_title']}.{page['mp_section']}"
        )

    return render_template("all.html", pages_by_distro=pages_by_distro)


@app.route("/<distro>/<page>.<section>")
def view_page(distro: str, section: int, page: str):
    with db.connection() as conn:
        result = conn.query_one(
            "SELECT mp_content FROM man_page WHERE mp_distro = %s "
            "AND mp_section = %s AND mp_title = %s;",
            [distro, section, page],
        )

    if not result:
        return "Not found", 404

    render = subprocess.Popen(
        [
            pathlib.Path(__file__).parent.joinpath("roffit").absolute(),
            "--bare",
        ],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )

    man_html = render.communicate(input=result["mp_content"].encode("utf-8"))[
        0
    ].decode()

    return render_template("man.html", html=man_html, title=page, section=section)


if __name__ == "__main__":
    app.run()
