import requests

USER_AGENT = (
    f"man-pages (tools.man-pages@toolforge.org) python-requests/{requests.__version__}"
)
