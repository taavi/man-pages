import gzip
import io
from collections import defaultdict

import requests
from debian import deb822, debfile, debian_support

from man_pages import USER_AGENT

MAN_BASE_PATH = "usr/share/man/"


class AptRepo:
    def __init__(self, base_url: str, distro: str):
        self.base_url = base_url
        self.distro = distro

        self.cached_data_files = {}
        self.architectures = None
        self.components = None

    def get_data_file(self, path: str):
        if path in self.cached_data_files:
            return self.cached_data_files[path]

        data = requests.get(
            f"{self.base_url}/dists/{self.distro}/{path}",
            headers={"User-Agent": USER_AGENT},
        )
        data.raise_for_status()

        if path.endswith("gz"):
            text = gzip.open(io.BytesIO(data.content)).read().decode("utf-8")
        else:
            text = data.text

        self.cached_data_files[path] = text
        return text

    def _parse_release(self):
        release = self.get_data_file("Release")
        for paragraph in deb822.Deb822.iter_paragraphs(release):
            for item in paragraph.items():
                if item[0] == "Architectures":
                    self.architectures = item[1].split(" ")
                elif item[0] == "Components":
                    self.components = item[1].split(" ")

    def get_architectures(self):
        if self.architectures is None:
            self._parse_release()
        return self.architectures

    def get_components(self):
        if self.components is None:
            self._parse_release()
        return self.components

    def get_deb_urls_with_man_pages(self):
        packages = defaultdict(lambda: set())
        for architecture in self.get_architectures():
            for component in self.get_components():
                contents = self.get_data_file(f"{component}/Contents-{architecture}.gz")
                for file in contents.split("\n"):
                    if not file.startswith(MAN_BASE_PATH):
                        continue
                    packages[component].add(file.split(" ")[1].split("/")[1])

        urls = dict()

        for (component, package_names) in packages.items():
            for architecture in self.get_architectures():
                pkg_data = self.get_data_file(
                    f"{component}/binary-{architecture}/Packages"
                )
                for paragraph in deb822.Deb822.iter_paragraphs(pkg_data):
                    items = dict(paragraph.items())
                    package_name = items["Package"]
                    if items["Package"] not in package_names:
                        continue
                    version = items["Version"]
                    deb_url = items["Filename"]

                    if package_name in urls:
                        existing_version = debian_support.Version(urls[package_name][0])
                        new_version = debian_support.Version(version)
                        if new_version < existing_version:
                            continue

                    urls[package_name] = (version, f"{self.base_url}/{deb_url}")

        return urls


def get_man_pages_for_deb(deb: str):
    archive = debfile.DebFile(deb)

    man = {}

    for file in archive.data:
        if not file.startswith(f"./{MAN_BASE_PATH}"):
            continue
        if not file.endswith(".gz"):
            # directories
            continue

        content = archive.data.get_file(file)
        gz = gzip.open(content)

        file_name = file.split("/")[-1].replace(".gz", "")
        man[file_name] = gz.read().decode("utf-8")

    return man
