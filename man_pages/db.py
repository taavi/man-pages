from contextlib import contextmanager

import pymysql


class Connection:
    def __init__(self):
        self.connection = None

    def connect(self):
        self.connection = pymysql.connect(
            db="s54884__manpages",
            user="s54884__manpages",
            password="1234",
            host="localhost",
            cursorclass=pymysql.cursors.DictCursor,
        )

    def query_one(self, sql: str, params=None):
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            return cursor.fetchone()

    def query_all(self, sql: str, params=None):
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            return cursor.fetchall()

    def update(self, sql: str, params=None):
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            self.connection.commit()


@contextmanager
def connection() -> Connection:
    conn = Connection()
    conn.connect()
    yield conn
    conn.connection.close()
