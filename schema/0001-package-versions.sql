CREATE TABLE package_versions (
    pv_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    pv_distro VARCHAR(255) NOT NULL,
    pv_package VARCHAR(255) NOT NULL,
    pv_last_version VARCHAR(255) NOT NULL
);
