CREATE TABLE man_page (
    mp_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    mp_distro VARCHAR(255) NOT NULL,
    mp_section TINYINT NOT NULL,
    mp_title VARCHAR(255) NOT NULL,
    mp_content TEXT NOT NULL,
    UNIQUE KEY mp_uniq(mp_distro, mp_section, mp_title)
);

