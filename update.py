import requests

from man_pages import apt, db, USER_AGENT
from tempfile import NamedTemporaryFile

connection = db.Connection()
connection.connect()


def do_update(distro: str, package_url: str):
    with NamedTemporaryFile() as tmpfile:
        tmpfile.write(requests.get(package_url, headers={"User-Agent": USER_AGENT}).content)

        pages = apt.get_man_pages_for_deb(tmpfile.name)

        for (page_title, page_content) in pages.items():
            name, section = page_title.split(".")
            section = int(section)
            connection.update(
                "INSERT INTO man_page (mp_distro, mp_section, mp_title, mp_content) VALUES (%s, %s, %s, %s)"
                " ON DUPLICATE KEY UPDATE mp_content = %s;",
                [distro, section, name, page_content, page_content]
            )


def update_for_distro(distro: str):
    page_urls = apt.AptRepo("https://deb-tools.wmcloud.org/repo", distro).get_deb_urls_with_man_pages()

    replacements = ','.join(['(%s, %s)'] * len(page_urls.keys()))

    params = [distro]
    for name, data in page_urls.items():
        params.append(name)
        params.append(data[0])

    up_to_date = connection.query_all(
        "SELECT pv_package FROM package_versions WHERE pv_distro = %s"
        + f"AND (pv_package, pv_last_version) IN ({replacements});",
        params
    )

    up_to_date_names = set([entry['pv_package'] for entry in up_to_date])
    to_update = set(page_urls.keys()) - up_to_date_names

    for package in to_update:
        package_version, package_url = page_urls[package]
        print(distro, package, package_version, package_url)
        do_update(distro, package_url)

        connection.update(
            "INSERT INTO package_versions (pv_distro, pv_package, pv_last_version) VALUES (%s, %s, %s)"
            "ON DUPLICATE KEY UPDATE pv_last_version = %s;",
            [distro, package, package_version, package_version]
        )


update_for_distro("stretch-tools")
update_for_distro("buster-tools")
update_for_distro("bullseye-tools")
